const {Truck}=require('./modules/Trucks.js');
const {User}=require('./modules/Users.js');
const {Load}=require('./modules/Loads.js');
const SPRINTER=[300, 250, 170, 1700];
const SMALL_STRAIGHT=[500, 250, 170, 2500];
const LARGE_STRAIGHT=[700, 350, 200, 4000];
const loadStateTransition=['En route to Pick Up', 'Arrived to Pick Up', 'On route to delivery', 'Arrived to delivery'];

async function createLoad (req, res, next) {
  try{  
    const user=await User.findById(req.user.userId);
    const {name, payload, pickup_address, delivery_address, dimensions}=req.body;
    const created_by =user._id;
    const assigned_to=false;
    const status='NEW';
    const state = 'false';
    const created_date= JSON.stringify(new Date());
    const logs =[];
    if(name && payload && pickup_address && delivery_address && dimensions){
        const load= await new Load({created_by, assigned_to, status, state, name, payload,
        pickup_address, delivery_address, dimensions, logs, created_date});
        load.save();
        res.status(200).send({ "message": "Success" });
    }else{
      res.status(400).send({ "message": "bad request" });
    }
  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }
}

async function getLoads (req, res, next) {
  try{
    const loads=await Load.find({"created_by":req.user.userId});
    res.status(200).json({"loads": loads});
  }catch(e){
    res.status(500).send({ "message": "internal server error"});
  }
}


async function updateLoad(req, res, next){
  try{  
    if(req.body){
      const load = await Load.findById(req.params.id);
      const {name, payload, pickup_address, delivery_address, dimensions}=req.body;
      if(req.user.userId==load.created_by && name && payload && pickup_address && delivery_address && dimensions && load.status=='NEW'){
        load.name=name;
        load.payload=payload;
        load.pickup_address=pickup_address;
        load.delivery_address=delivery_address;
        load.dimensions=dimensions;
        load.save();
        res.status(200).send({ "message": "Success" });
    }else{
      res.status(400).send({ "message": "bad request" });
    }
  }

  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }
}

async function deleteLoad(req, res, next){
  try{  
    if(req.body){
      const load = await Load.findById(req.params.id);
      if(load.status=='NEW' && req.user.userId==load.created_by){
        load.delete();
        res.status(200).send({ "message": "Load deleted successfully"});
      }else{
      res.status(400).send({ "message": "bad request" });
        }
    }
  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }
}

async function loadsInfo(req, res, next){
    try{  
        const load= await Load.findById(req.params.id);
      if(load.created_by===req.user.userId){
        const truck = await Truck.find({"assigned_to": load.assigned_to});
        res.status(200).send({ "message": "success", "load": load, "truck": truck});
      }else{
        res.status(400).send({ "message": "bad request" });
      }
    }catch(e){
      res.status(500).send({ "message": "internal server error" });
    }
  }

  async function driversActiveLoad(req, res, next){
    try{  
        const load= await Load.findOne({"assigned_to": req.user.userId});
      if(load){        
        res.status(200).send({ "message": "success", "load": load, "id":req.user.userId});
      }else{
        res.status(400).send({ "message": "bad request", "id":req.user.userId});
      }
    }catch(e){
      res.status(500).send({ "message": "internal server error" });
    }
  }

  
  async function iterateLoadToNextStage(req, res, next){
    try{  
        let load= await Load.find({"assigned_to": req.user.userId});
        const truck = await Truck.find({"assigned_to": req.user.userId});
      if(load && truck){ 
        for (let i=0; i<loadStateTransition.length; i++){ 
            console.log(loadStateTransition[i]);
            console.log(load[0].state);
            if (load[0].state==loadStateTransition[loadStateTransition.length-2]){
                truck[0].status='IO';
                truck[0].assigned_to='false';
                load[0].state=loadStateTransition[loadStateTransition.length-1];
                load[0].status='SHIPPED';
                console.log('!!')
                break;
            }else if(load[0].state==loadStateTransition[i]){
                load[0].state=loadStateTransition[i+1];
                console.log('!')
                break;
            }
        }
        load[0].save();
        truck[0].save();     
        res.status(200).send({ "message": `Load state changed to ${load[0].state}`});
      }else{
        res.status(400).send({ "message": "bad request" });
      }
    }catch(e){
      res.status(500).send({ "message": "internal server error" });
    }
  }
  

async function postLoad(req, res, next){
    try{  
        let flag=true;
        const load = await Load.findById(req.params.id);
        const trucks = await Truck.find({"status":"IS"});
        let truckForOrder;        
        for (let i=0; i<trucks.length; i++){
            if(trucks[i].type==='SPRINTER'){
                let weight=SPRINTER[3];
                let width=SPRINTER[0];
                let length=SPRINTER[1];
                let height=SPRINTER[2];
                if (!(trucks[i].assigned_to=='false') && load.payload<=weight && load.dimensions.width<=width && load.dimensions.length<=length && load.dimensions.height<=height){
                    truckForOrder=trucks[i];
                    break;
                }else{
                    continue;
                }
            }else if(trucks[i].type==='SMALL STRAIGHT'){
                let weight=SMALL_STRAIGHT[3];
                let width=SMALL_STRAIGHT[0];
                let length=SMALL_STRAIGHT[1];
                let height=SMALL_STRAIGHT[2];
                if (!(trucks[i].assigned_to=='false') && load.payload<=weight && load.dimensions.width<=width && load.dimensions.length<=length && load.dimensions.height<=height){
                    truckForOrder=trucks[i];
                    break;
                }else{
                    continue;
                }

            }else{
              console.log(3);
                let weight=LARGE_STRAIGHT[3];
                let width=LARGE_STRAIGHT[0];
                let length=LARGE_STRAIGHT[1];
                let height=LARGE_STRAIGHT[2];
                if (!(trucks[i].assigned_to=='false') && load.payload<=weight && load.dimensions.width<=width && load.dimensions.length<=length && load.dimensions.height<=height){
                    truckForOrder=trucks[i];
                    break;
                }else{
                    continue;
                }
            }
        }
        if(truckForOrder && load.status==='NEW'){
            truckForOrder.status='OL';
            load.status='ASSIGNED';
            load.assigned_to=truckForOrder.assigned_to;
            load.logs.push({message:'Load assigned to the driver', time: (new Date()).toString});
            load.state='En route to Pick Up';
            load.save();
            truckForOrder.save();
            res.status(200).send({ "message": "Load posted successfully", "driver_found": true});
        }else{
            load.logs.push({message:'Load was not assigned to the driver', time: new Date()});
            load.save();
            res.status(400).send({ "message": "bad request", "status": load.status, "truck": truckForOrder});
        }
    }catch(e){
      res.status(500).send({ "message": "internal server error" });
    }
  }
   
 

module.exports = {
    createLoad,
    getLoads,
    updateLoad,
    deleteLoad,
    postLoad,
    loadsInfo,
    driversActiveLoad,
    iterateLoadToNextStage
}
