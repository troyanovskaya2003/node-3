const express = require('express');
const router = express.Router();
const { createLoad, getLoads, updateLoad, deleteLoad, postLoad, loadsInfo, driversActiveLoad, iterateLoadToNextStage} = require('./loadsService.js');
const {authMiddleware, isDriver, isShipper}=require('./middleware/authMiddleware.js');

router.post('/loads', authMiddleware, isShipper, createLoad);

router.get('/loads', authMiddleware, isShipper, getLoads);

router.post('/loads/:id/post', authMiddleware, isShipper, postLoad);

router.put('/loads/:id', authMiddleware, isShipper, updateLoad);

router.delete('/loads/:id', authMiddleware, isShipper, deleteLoad);

router.get('/loads/:id/shipping_info', authMiddleware, isShipper, loadsInfo);

router.get('/loads/active', authMiddleware, isDriver, driversActiveLoad);

router.patch('/loads/active/state', authMiddleware, isDriver, iterateLoadToNextStage);

module.exports = {
  loadsRouter: router
};