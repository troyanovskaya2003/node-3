const {User}=require('../modules/Users.js');
const jwt=require('jsonwebtoken');

function authMiddleware(req, res, next){
    const {authorization}=req.headers;
    if(!authorization){
        return res.status(401).json({"message":"Please, provide valid header"});
    }
    const [, token]=authorization.split(' ');
    if(!token){
        next(new Error('Please, include token to request'));
    }
    try{
        const tokenPayLoad=jwt.verify(token, 'secret-key');
        req.user={userId:tokenPayLoad.userId, email:tokenPayLoad.email};
        next();
    }catch(e){
        res.status(401).json({message: "Something bad happened"});
    }
}

async function isShipper(req, res, next){
    try{
        const user=await User.findById(req.user.userId);
        if(user.role==='SHIPPER'){
            next();
        }else{
            next(new Error('Drivers are not eligible to delete profile'));
        }
    }catch(e){
        console.log(4);
        res.status(401).json({message: "Something bad happened"});
    }
}

async function isDriver(req, res, next){
    try{
        const user=await User.findById(req.user.userId);

        if(user.role==='DRIVER'){

            next();
        }else{
            next(new Error('Drivers are not eligible to delete profile'));
        }
    }catch(e){
        res.status(401).json({message: "Something bad happened"});
    }
}



module.exports={
    authMiddleware,
    isShipper,
    isDriver
}
