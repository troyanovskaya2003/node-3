const express = require('express');
const router = express.Router();
const { createTruck, getTrucks, assignTruck, updateTruck, deleteTruck} = require('./trucksService.js');
const {authMiddleware, isDriver, isShipper}=require('./middleware/authMiddleware.js');

router.post('/trucks', authMiddleware, isDriver,  createTruck);

router.get('/trucks', authMiddleware, isDriver, getTrucks);

router.post('/trucks/:id/assign', authMiddleware, isDriver, assignTruck);

router.put('/trucks/:id', authMiddleware, isDriver, updateTruck);

router.delete('/trucks/:id', authMiddleware, isDriver, deleteTruck);

module.exports = {
  trucksRouter: router
};
