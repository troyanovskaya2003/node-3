const {User}=require('./modules/Users.js');
const jwt=require('jsonwebtoken');
const bcryptjs=require('bcryptjs');

async function registerUser(req, res, next){
    try{
        let created_date= JSON.stringify(new Date());
        const {email, password, role}=req.body;
        if(email && password && (role.toUpperCase=='DRIVER'||'SHIPPER')){
            const user=new User({
                role,
                email,
                created_date,
                password: await bcryptjs.hash(password, 10)
            });
            user.save();
            res.status(200).send({"message": "Profile created successfully"});
        }else{
            res.status(400).json({"message": "bad request"});
        }        
    }catch(e){
        res.status(500).send({"message": "eternal server error"});
    }
    
}
async function loginUser(req, res, next){ 
    try{
        const user=await User.findOne({email: req.body.email});
        if(user && await bcryptjs.compare(String(req.body.password), String(user.password))){
            const payload={email:user.email, userId:user._id};
            const jwtToken=jwt.sign(payload,'secret-key');
            res.status(200).json({"jwt_token":jwtToken, "message":"success"});
        }else{
            res.status(400).send({"message": "bad request"}); 
        }
    }catch(e){
        res.status(500).send({"message": "eternal server error"});
    }
}
async function getUsersInfo(req, res, next){
    try{
      const user=await User.findById(req.user.userId);
      if(user){
        res.status(200).json({user:{"_id": user._id, "email":user.email, "creationDate":user.created_date, "role":user.role}});
      }else{
        res.status(400).send({"message": "bad request"});
      }
      
    }catch(e){
        res.status(500).send({"message": "eternal server error"});
    }    
}

async function deleteUser(req, res, next){
    try{
        const user=await User.findById(req.user.userId);
        user.delete();
        const user1=await User.findById(req.user.userId);
        if(!user1){
          res.status(200).send({"message":"Profile deleted successfully"});
        }else{
          res.status(400).send({"message": "bad request"});
        }        
      }catch(e){
          res.status(500).send({"message": "eternal server error"});
      }
}

async function changeUsersPassword(req, res, next){
    try{
        const oldPassword=req.body.oldPassword;
        const newPassword=await bcryptjs.hash(req.body.newPassword, 10);
        const user=await User.findById(req.user.userId);
        const p=await bcryptjs.compare(user.password, oldPassword);
        if(await bcryptjs.compare(oldPassword, user.password)){
            user.password=newPassword;
            user.save();
            res.status(200).send({"message":"success"});
        }else{
            res.status(400).send({"message": "bad request"}); 
        }
    }catch(e){
        res.status(500).send({"message": "eternal server error"});
    }

}

module.exports = {
    registerUser,
    loginUser,
    getUsersInfo,
    deleteUser,
    changeUsersPassword
}