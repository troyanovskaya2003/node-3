const mongoose=require('mongoose');
mongoose.connect('mongodb+srv://Ann:fndt75DSk@cluster0.4skxzza.mongodb.net/Uber?retryWrites=true&w=majority');
const express = require('express');
const morgan = require('morgan')
const app = express();
const port=8080;

const { trucksRouter } = require('./trucksRouter.js');
const { usersRouter } = require('./usersRouter.js');
const { loadsRouter } = require('./loadsRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/', trucksRouter);
app.use('/api/', usersRouter);
app.use('/api/', loadsRouter);

app.listen(port);

//ERROR HANDLER
app.use(errorHandler)

function errorHandler (err, req, res, next) {
  console.error('err')
  res.status(500).send({'message': 'Server error1'});
}
