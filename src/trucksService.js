const {Truck}=require('./modules/Trucks.js');
const {User}=require('./modules/Users.js');

async function createTruck (req, res, next) {
  try{  
    const user=await User.findById(req.user.userId);
    const {type}=req.body;
    const created_by =user._id;
    const assigned_to=false;
    const status='IS';
    const created_date= JSON.stringify(new Date());
    if(type){
      const truck= await new Truck({created_by, assigned_to, type, status, created_date});
      truck.save();
      res.status(200).send({ "message": "Success" });
    }else{
      res.status(400).send({ "message": "bad request" });
    }

  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }

}

async function getTrucks (req, res, next) {
  try{
    const trucks=await Truck.find({"created_by":req.user.userId});
    res.status(200).json({"trucks": trucks});
  }catch(e){
    res.status(500).send({ "message": "internal server error"});
  }
}

async function assignTruck (req, res, next) {
  try{
    const truck=await Truck.findById(req.params.id);
    const truck1 = await Truck.findOne({"assigned_to":req.params.id})
    if(truck && !truck1){
      truck.assigned_to=req.user.userId;
      truck.save();
      res.status(200).send({"message": "Truck assigned successfully"});
    }else{
      res.status(400).send({ "message": "bad request" }); 
    }

  }catch(e){
    res.status(500).send({ "message": "internal server error"});
  }
}


async function updateTruck(req, res, next){
  try{  
    if(req.body){
      const truck = await Truck.findById(req.params.id);
      const {type}=req.body;
      if(truck.assigned_to!==req.user.userId && truck.status=='IO'){
        truck.type=type;
        truck.save();
        res.status(200).send({ "message": "Truck details changed successfully"});
      }else{
      res.status(400).send({ "message": "bad request" });
    }
  }

  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }
}

async function deleteTruck(req, res, next){
  try{  
    if(req.body){
      const truck = await Truck.findById(req.params.id);
      if(truck.assigned_to!==req.user.userId){
        truck.delete();
        res.status(200).send({ "message": "Truck deleted successfully"});
      }else{
      res.status(400).send({ "message": "bad request" });
    }
  }

  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }
}

module.exports = {
  createTruck,
  getTrucks,
  assignTruck,
  updateTruck,
  deleteTruck
}
