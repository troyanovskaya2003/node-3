const express = require('express');
const path = require('path');
const router = express.Router();
const {registerUser, loginUser, getUsersInfo, deleteUser, changeUsersPassword} = require('./usersService.js');
const {authMiddleware, isDriver, isShipper}=require('./middleware/authMiddleware.js');


router.post('/auth/register', registerUser);
router.post('/auth/login', loginUser);
router.get('/users/me', authMiddleware, getUsersInfo);
router.delete('/users/me', authMiddleware, isDriver, deleteUser);
router.patch('/users/me', authMiddleware, changeUsersPassword);

module.exports = {
    usersRouter: router
};