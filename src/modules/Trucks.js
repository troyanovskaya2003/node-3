const mongoose=require('mongoose');

const truckSchema=mongoose.Schema({
  created_by:{
    type: String,
    required:true
  },
  assigned_to:{
    type: String,
    required:true
  },
  type:{
    type: String,
    required:true
  },
  status:{
    type: String,
    required:true
  },
  created_date:{
    type: String,
    required:true
  }
});

const Truck=mongoose.model('Truck', truckSchema);
module.exports={
    Truck
}