const mongoose=require('mongoose');
//mongoose.connect('mongodb+srv://Ann:fndt75DSk@cluster0.4skxzza.mongodb.net/Uber?retryWrites=true&w=majority');

const loadSchema=mongoose.Schema({
    created_by:{
        type: String,
        required:true
    },
    assigned_to:{
        type: String,
        required:true
    },
    status:{
        type: String,
        required:false
    },
    state:{
        type: String,
        required:true
    },
    name:{
        type: String,
        required:true
      },	
    payload	:{
        type: Number,
        required:true
      },
    pickup_address:{
        type: String,
        required:true
      },	
    delivery_address:{
        type: String,
        required:true
      },	
    dimensions:{
        type: Object,
        required:true
      },	
    logs:{
        type: Array,
        required:true
       },
    created_date:{
    type: String,
    required:true
  }
});

const Load=mongoose.model('Load', loadSchema);
module.exports={
    Load
}