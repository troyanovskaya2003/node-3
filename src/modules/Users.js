const mongoose=require('mongoose');

const userSchema=mongoose.Schema({
  role:{
    type: String,
    required:true
  },
  email:{
    type: String,
    required:true
  },
  created_date:{
    type:String,
    required:true
  },
  password:{
    type:String,
    required:true
  }

});
const User=mongoose.model('User', userSchema);
module.exports={
    User
}
// let alla=new User({name:'Alla122454', password:'00000', date: new Date()})
// alla.save().then(data=>console.log(data));
// User.updateOne({name:'Alla'}, {$set:{name:'Nikolya'}}).then(update=>console.log(update)).catch(console.error);