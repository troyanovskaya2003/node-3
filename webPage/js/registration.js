document.querySelector('#btn-submit-2').addEventListener('click', registerInSystem);
const url='http://localhost:8080/api/auth/register';
//const axios = require('axios').default;

function register(email, password, role){
    fetch(url, {     
        method: "POST",         
        body: JSON.stringify({
           'email': email,
            'password': password,
            'role': role
        }),         
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods' : 'POST,GET,OPTIONS,PUT,DELETE',
            "Content-type": "application/json; charset=UTF-8"
        }
    }).then(data=>{
    if (data.status==200){
        window.location.assign('http://127.0.0.1:8080//webPage/indexLogin.html');
    }else{
        alert('This login is already in use')
    }});
}

function registerInSystem(e){
    e.preventDefault();
    const email = document.querySelector('#registration-login').value;
    const password = document.querySelector('#registration-password').value;
    const password_2 = document.querySelector('#registration-password-2').value;
    const role=document.querySelector('#registration-role').value;
    if(email && password && password_2 && role){
        if(password===password_2){
            if(role.toUpperCase()=='DRIVER'||role.toUpperCase()=='SHIPPER'){
                register(email, password, role.toUpperCase());
            }else{
                alert('Please, enter your role as a shipper or driver');   
            }
        }else{
            alert('Please, enter the same password again');
        }
    }else{
        alert('Please, fill all fields');
    }
}